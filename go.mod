module github.com/nortonlifelock/aws

go 1.13

require (
	github.com/aws/aws-sdk-go v1.28.9
	github.com/nortonlifelock/aegis v1.0.1-0.20200214181401-3a5d0fe8c62c // indirect
)
